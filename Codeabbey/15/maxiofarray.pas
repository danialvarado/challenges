program maxiofarray;
uses classes, sysutils;


var
  input: string;
  i, j, index:Longint;
  list: TStringList;
  vector:array[0..299] of AnsiString;

begin
   writeln('input data:');
   readln(input);
   writeln(input);
   list := TStringList.Create;
   list.DeLimiter := ' ';
   list.DeLimitedText := input;
   vector := StrToInt(list);
   for i:=2 to 299 do
     begin
       index :=list[i];
       j := i;
       while ((j > 1) and (StrToInt(list[j-1]) > index)) do
         begin
           list[j] := list[j-1];
           j := j -1;
         end;
       list[j] := index;
     end;
   writeln('answer:');
   writeln(list[0], ' ', list[299]);
   readln;            end.