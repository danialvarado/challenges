program mini;

uses classes, sysutils;
type a = array of Longint;
var
  x, y, z: Longint;
  minis: a;
  i: integer;

begin
  writeln('datos de entrada:');
  readln(x);
  writeln(x);
  setLength(minis, x);
  for i:=0 to x-1 do
    begin
      readln(z, y);
      writeln(z, ' ', y);
      if (z < y) then
        minis[i] := z
      else
        minis[i] := y;
    end;
  writeln('respuesta:');
  for i:=0 to x-1 do
     write(minis[i], ' ');
  readln;
end.
