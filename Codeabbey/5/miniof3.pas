program miniof3;

uses classes, sysutils;
type a = array of Longint;
var
  x, y, z, w: Longint;
  minis: a;
  i: integer;

begin
  writeln('datos de entrada:');
  readln(x);
  writeln(x);
  setLength(minis, x);
  for i:=0 to x-1 do
    begin
      readln(z, y, w);
      writeln(z, ' ', y, ' ', w);
      if (z < y)and(z < w) then
        minis[i] := z
      else if (y < z)and (y< w) then
        minis[i] := y
      else
        minis[i] := w;
    end;
  writeln('respuesta:');
  for i:=0 to x-1 do
     write(minis[i], ' ');
  readln;
end.