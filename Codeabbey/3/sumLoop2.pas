program sumLoop2;

uses classes, sysutils;

type a= array of Longint;
var
  answers: a;
  x, y, z: Longint;
  i:Longint;

begin
  writeln('datos:');
  readln(x);
  writeln(x);
  setLength(answers, x);
  for i:=0 to x-1 do
    begin
      readln(z, y);
      writeln(z, ' ', y);
      answers[i] := z+y;
    end;
  writeln('respuesta:');
  for i:=0 to x-1 do
    write(answers[i], ' ');
  readln;
end.